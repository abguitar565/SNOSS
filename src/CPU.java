import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;


public class CPU {
	
	CPURegister[] registers;
	RAM ram;
	int instructionPointer = 0;
	int PCB = 0; //Look here for introducing multiple processes TODO 
	boolean running = true;
	
	public CPU(){
		this.registers = new CPURegister[6];
		for (int i = 0; i < 6; i++) {
			registers[i] = new CPURegister();
			
		}
		ram = new RAM();
	}
	
	public void add(int outputR, int inputr, int inputr2){
		//breakpoint to show prof on registers line
		registers[outputR-1].value = registers[inputr-1].value + registers[inputr2-1].value; 
		instructionPointer+=4;
	}
	public void sub(int outputR, int valReg, int differenceReg){
		registers[outputR-1].value = registers[valReg-1].value - registers[differenceReg-1].value;
		instructionPointer+=4;
	}
	public void mul(int outPutR, int inputr, int inputr2){
		registers[outPutR-1].value = registers[inputr-1].value * registers[inputr2-1].value;
		instructionPointer+=4;
	}
	public void div(int outputR, int valReg, int dividor){
		registers[outputR-1].value = registers[valReg-1].value/registers[dividor-1].value;
		instructionPointer+=4;
	}
	public void eq(int outputR, int valReg, int dividor){
		if(registers[valReg-1].value==registers[dividor-1].value){
			registers[outputR-1].value = 1;
		}
		instructionPointer+=4;
	}
	public void load(int register, int val){
		registers[register-1].value = ram.memory[val];
		instructionPointer+=4;
	}
	public void loadc(int register, int val){
		registers[register-1].value = val;
		instructionPointer+=4;
	}
	public void store(int memAdd, int reg){
		//Store TODO storing a registers value to memory could possibly overwrite instructions, since they share mem space?
		ram.memory[memAdd] = (byte) registers[reg-1].value;
		instructionPointer+=4;
	}
	private void goTO(int memAdd) {
		instructionPointer = memAdd;
	}
	private void goTOIF(int memAdd, int regNumForBool){
		if(registers[regNumForBool-1].value>0){
			instructionPointer = memAdd;
		}
	}
	public void cprint(int memAdd){
		System.out.println(ram.memory[memAdd]);
		instructionPointer+=4;
	}
	public void cread(int memAdd){
		Scanner sc = new Scanner(System.in);
		System.out.println("Give a byte for cread");
		String tempInput = sc.next();
		int farenheit = Integer.parseInt(tempInput);
		ram.memory[memAdd] = (byte) farenheit; //TODO this causes an error when capturing input
		instructionPointer+=4;
		sc.close();
	}
	public void cdump(){
		System.out.println("=========COREDUMP==========");
		for (int i = 0; i < registers.length; i++) {
			System.out.println("Register "+(i+1)+":"+registers[i].value);
		}
		ram.printUsedMemory();
		System.out.println("Instruction Pointer: "+instructionPointer);
		running = false;
	}
	public void run(boolean execimode) throws IOException{
		loadIntoRAM();
	
		//loop this
		while(running){
			int[] currentInstruction = Arrays.copyOfRange(ram.memory, instructionPointer+PCB, instructionPointer+PCB+4); //gives one instruction, aka 4 bytes from instructionpointer
			switch (currentInstruction[0]) {
			case 1:
				load(currentInstruction[1], currentInstruction[2]); // 2 needs to be 2 and 3
				break;
			case 10:
				loadc(currentInstruction[1], currentInstruction[2]);
				break;
			case 2:
				store(currentInstruction[1], currentInstruction[2]);
				break;
			case 3:
				add(currentInstruction[1], currentInstruction[2], currentInstruction[3]);
				break;
			case 4:
				sub(currentInstruction[1], currentInstruction[2], currentInstruction[3]);
				break;
			case 5:
				mul(currentInstruction[1], currentInstruction[2], currentInstruction[3]);
				break;
			case 6:
				div(currentInstruction[1], currentInstruction[2], currentInstruction[3]);
				break;
			case 7:
				eq(currentInstruction[1], currentInstruction[2], currentInstruction[3]);
				break;
			case 8:
				goTO(currentInstruction[1]);
				break;
			case 11:
				goTOIF(currentInstruction[1],currentInstruction[2]);
				break;
			case 9:
				cprint(currentInstruction[1]);
				break;
			case 16:
				cread(currentInstruction[1]);
				break;
			case 17:
				running = false;
				break;
			case 18:
				cdump();
				break;
			default:
				break;
			}
			if(execimode){
				coreDump();
			}
		}
		
	}
	
	public void loadIntoRAM() throws FileNotFoundException, IOException {
		FileInputStream fis = new FileInputStream(new File("arushcode.ab"));
		byte[] buffer = new byte[4];
		while(fis.read(buffer)!= -1){
			int i=buffer[0];
			System.out.println("Fn Code is: "+i);
			ram.addToStack(buffer);
		}
	}
	
	public void coreDump(){
		System.out.println("PCB: "+PCB);
		System.out.println("Instruction Poitner: "+instructionPointer);
		for (int i = 0; i < ram.memory.length; i++) {
			System.out.println("Memory Address "+i+" : "+ram.memory[i]);
		}
	}
}
