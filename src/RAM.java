
public class RAM {
	
	public int[] memory;
	public int currentMemPointer;
	
	public RAM(){
		memory = new int[10000];
		//500+
		
	}
	
	public void addToStack(byte[] newData){
		//this correctly adds to memory stack
		for (int i = currentMemPointer; i < newData.length+currentMemPointer; i++) {
			memory[i] = newData[i-currentMemPointer];
		}
		currentMemPointer += newData.length;
	}
	
	public void bulkAddMemory(byte[] b){
		memory = new int[10000];
		for (int i = 0; i < b.length; i++) {
			memory[i] = b[i];
		}
	}
	
	public void printUsedMemory(){
		for (int i = 0; i < currentMemPointer+100; i++) {
			System.out.println("Memory Address "+i+": "+memory[i]);
		}
		
	}
	
}
