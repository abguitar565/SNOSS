import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;


public class OS {
	public static void main(String[] args) throws IOException {
		boolean stillRunning = true;
		ReadIn rin = new ReadIn();
		Scanner sc = new Scanner(System.in);
		CPU cpu = new CPU();
		System.out.println("Welcome to ArushOS");
//		System.out.println(" 1. exec");
//		System.out.println(" 2. exec_i");
//		System.out.println(" 3. ps");
//		System.out.println(" 4. ls");
//		System.out.println(" 5. kill");
//		System.out.println(" 6. exit");
		
		while(stillRunning){
			String line = sc.nextLine();
			String[] datas = line.split(" ");
			String lineCommand = datas[0];
			if(lineCommand.equalsIgnoreCase("exec")){//exec arushCodeAssemble.txt
				stillRunning = false;
				rin.converToBinaryInstructions(datas[1]);
				try {
					cpu.run(false);
				} catch (Exception e) {
					e.printStackTrace();
					cpu.cdump();
					
				}
				
			}else if(lineCommand.equalsIgnoreCase("exec_i")){//exec arushCodeAssemble.txt
				stillRunning = false;
				rin.converToBinaryInstructions(datas[1]);
				try {
					cpu.run(true);
				} catch (Exception e) {
					e.printStackTrace();
					cpu.cdump();
				}
			}
			else if(lineCommand.equalsIgnoreCase("ls")){
				System.out.println("arushcodeDump.txt");
				System.out.println("arushcodeFTOC.txt");
				System.out.println("arushcodeFib.txt");
			}else if(lineCommand.equalsIgnoreCase("ps")){
				if(cpu.running){
					System.out.println(1);
				}else{
					System.out.println(0);
				}
			}else if(lineCommand.equalsIgnoreCase("exit")){
				stillRunning = false;
			}else if(lineCommand.equalsIgnoreCase("kill")){
				cpu.running = false;
			}
		}
		sc.close();
	}
}
