import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;


public class ReadIn {
	BinaryVal b;
	File f;
	FileOutputStream fos;
	DataOutputStream dos;
	
	public ReadIn() throws FileNotFoundException{
		b =  new BinaryVal();
		fos = new FileOutputStream("arushcode.ab");
		dos = new DataOutputStream(fos);
	}
	
	public void converToBinaryInstructions(String fileName) throws IOException{
		f = new File(fileName);
		
		String line;
		BufferedReader bReader = new BufferedReader(new FileReader(f));

		
		while((line = bReader.readLine())!= null){
			b.clean();
			System.out.println(line);
			String parsedLine = line.replaceAll(",", "");
			String[] splitValues = parsedLine.split(" ");
			//Instruction Table big ifs
			//loads the correct ints tht correspond with the 4 bytes in the Assembly to Binary Table
			
			switch (splitValues[0]) {
			case "LOAD":
				b.byte1 = 1;
				b.byte2 = Byte.parseByte(splitValues[1].split("R")[1]);
				b.byte3 = Byte.parseByte(splitValues[2].split("x")[1], 16);
				//split short into byte 3 and 4 
				break;
			case "LOADC":
				b.byte1 = 10;
				b.byte2 = Byte.parseByte(splitValues[1].split("R")[1]);
				b.byte3 = Byte.parseByte(splitValues[2]);
				break;
			case "STORE":
				b.byte1 = 2;
				b.byte2 = Byte.parseByte(splitValues[1].split("x")[1], 16);
				b.byte3 = Byte.parseByte(splitValues[2].split("R")[1]);
				break;
			case "ADD":
				b.byte1 = 3;
				b.byte2 = Byte.parseByte(splitValues[1].split("R")[1]);
				b.byte3 = Byte.parseByte(splitValues[2].split("R")[1]);
				b.byte4 = Byte.parseByte(splitValues[3].split("R")[1]);
				break;
			case "SUB":
				b.byte1 = 4;
				b.byte2 = Byte.parseByte(splitValues[1].split("R")[1]);
				b.byte3 = Byte.parseByte(splitValues[2].split("R")[1]);
				b.byte4 = Byte.parseByte(splitValues[3].split("R")[1]);
				break;
			case "MUL":
				b.byte1 = 5;
				b.byte2 = Byte.parseByte(splitValues[1].split("R")[1]);
				b.byte3 = Byte.parseByte(splitValues[2].split("R")[1]);
				b.byte4 = Byte.parseByte(splitValues[3].split("R")[1]);
				break;
			case "DIV":
				b.byte1 = 6;
				b.byte2 = Byte.parseByte(splitValues[1].split("R")[1]);
				b.byte3 = Byte.parseByte(splitValues[2].split("R")[1]);
				b.byte4 = Byte.parseByte(splitValues[3].split("R")[1]);
				break;
			case "EQ":
				b.byte1 = 7;
				b.byte2 = Byte.parseByte(splitValues[1].split("R")[1]);
				b.byte3 = Byte.parseByte(splitValues[2].split("R")[1]);
				b.byte4 = Byte.parseByte(splitValues[3].split("R")[1]);
				break;
			case "GOTO":
				b.byte1 = 8;
				b.byte2 = Byte.parseByte(splitValues[1].split("x")[1], 16);
				break;
			case "GOTOIF":
				b.byte1 = 11;
				b.byte2 = Byte.parseByte(splitValues[1].split("x")[1], 16);
				b.byte3 = Byte.parseByte(splitValues[2].split("R")[1]);
				break;
			case "CPRINT":
				b.byte1 = 9;
				b.byte2 = Byte.parseByte(splitValues[1].split("x")[1], 16);
				break;
			case "CREAD":
				b.byte1 = 16;
				b.byte2 = Byte.parseByte(splitValues[1].split("x")[1], 16);
				break;
			case "EXIT":
				b.byte1 = 17;
				break;
			case "CDUMP":
				b.byte1 = 18;
				break;
			default:
				break;
			}
			//print to binary file the bytes
			byte[] test = b.convertToBinary();
			fos.write(b.convertToBinary());
			fos.flush();
			
		}
		
	}

}
